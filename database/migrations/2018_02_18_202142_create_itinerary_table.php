<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItineraryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itinerary', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('tour_id')->unsigned();
            $table->text('image');
			$table->string('time');
			$table->string('title');
			$table->string('subtitle');
			$table->text('description');
			$table->text('highlightIcon');
			$table->string('highlightTitle');
			$table->string('highlightShortDescription');

            $table->timestamps();

            $table->foreign('tour_id')->references('id')->on('tours')
	            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itinerary');
    }
}
