<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
	        $table->increments('id');
	        $table->integer('slideShow_id')->unsigned();
	        $table->string('title');
	        $table->string('slug')->unique();
	        $table->string('subtitle');
	        $table->text('description');
	        $table->text('image');
	        $table->timestamps();

	        $table->foreign('slideShow_id')->references('id')->on('slideShows');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
