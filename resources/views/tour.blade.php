@extends('layouts.frontend')
@section('content')

    <!--  :::  HEADER ::: -->
    @include('tourBits.header')
    <!-- ::: HOME SLIDER END ::: -->

    <!--  :::  HOME SLIDER ::: -->
    @include('tourBits.home-slider')
    <!-- ::: HOME SLIDER END ::: -->

    <!--  :::  DESTINATION INFO ::: -->
    @include('tourBits.destination-info')
    <!-- ::: END ::: -->

    <!--  :::  MOBILE SUBSCRIBE FORM ::: -->
    @include('tourBits.mobile-subscribe-form')
    <!-- ::: MOBILE SUBSCRIBE FORM END ::: -->

    <!--  :::  TRAVEL INFO ::: -->
    @include('tourBits.travel-info')
    <!-- ::: END ::: -->

    <!--  ::: ITINARARY START ::: -->
    @include('tourBits.itinerary')
    <!-- ::: ITINARARY END ::: -->

    <!--  ::: GALLERY START ::: -->
    @include('tourBits.gallery')
    <!-- ::: GALLERY END ::: -->

    <!--  ::: LINED UP TOURS ::: -->
    @include('tourBits.lined-up-tours')
    <!-- ::: UPCOMING END ::: -->

    <!-- ::: START TESTIMONIALS ::: -->
    @include('tourBits.testimonials')
    <!-- ::: END ::: -->

    <!--  ::: ABOUT COMPANY ::: -->
    @include('tourBits.about-company')
    <!-- ::: END ::: -->

    <!--  ::: CONTACT ::: -->
    @include('tourBits.contact')
    <!-- ::: END ::: -->

    <!-- ::: Terms & Conditions Content ::: -->
    @include('tourBits.tos')
    <!-- ::: Terms end ::: -->

@endsection
