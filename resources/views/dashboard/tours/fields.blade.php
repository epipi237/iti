<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <i class="icon-calendar"></i>
                <h3 class="panel-title">Tour basic info</h3>
            </div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12 col-sm-12">
                        <label for="name">Name</label>
                        <input type="text" class="form-control input-lg" placeholder="Tour name" id="name" name="name" value="{{ old('name') }}">
                        <label for="slug">Slug (url)</label>
                        <input type="text" class="form-control input-sm" placeholder="tour-url" id="slug" name="slug" value="{{ old('slug') }}">
                        <label for="subtitle">Subtitle</label>
                        <input type="text" class="form-control " placeholder="Subtitle" id="subtitle" name="subtitle" value="{{ old('subtitle') }}">
                        <label for="description">Description</label>
                        <textarea id="description" name="description" >{{ old('description') }}</textarea>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Thumbnail image
                </div>
                <div class="panel-body">
                    <label class="btn btn-default" for="image">
                        <input id="image" type="file" style="display:none" onchange="$('#upload-file-info').html(this.files[0].name)">
                        Upload image
                    </label>
                    <span id="upload-file-info"></span>
                </div>
            </div>
        </div>
    </div>
</div>