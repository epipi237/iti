@extends('layouts.dashboard')
@section('headScripts')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
@endsection
@section('breadcrumb')
    -> New Tour
@endsection
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <form action="{{ route('tours.create') }}" method="post">
        {{ csrf_field() }}
        @include('dashboard.tours.fields')
        <button type="submit" class="btn btn-primary pull-right">Save</button>
    </form>
@endsection