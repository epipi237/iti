<section class="page-block">
    <div class="container">
        <div class="row">

            <h1 class="text-center wow zoomIn" data-wow-duration="0.50s" data-wow-delay="0.50s">Lined <span>Up</span><small>Our Upcoming Tours</small></h1>
            <div class="col-md-12">

                <!-- ::: Upcoming Start ::: -->
                <div id="upcoming">

                    <div class="item wow zoomIn" data-wow-duration="0.50s" data-wow-delay="0.50s">
                        <div class="name">
                            <h1>Wild Life <span>Safari</span></h1>
                            <a class="link" href="#" title=""><i class="fa fa-link"></i></a>
                            <h4>25 JAN<span>7 Days &amp; 6 Nights</span></h4>
                        </div>
                        <img src="images/gallery/small/01.jpg" alt="">
                    </div>

                    <div class="item wow zoomIn" data-wow-duration="0.50s" data-wow-delay="0.65s">
                        <div class="name">
                            <h1>Honeymoon <span>Tour</span></h1>
                            <a class="link" href="#" title=""><i class="fa fa-link"></i></a>
                            <h4>25 JAN<span>7 Days &amp; 6 Nights</span></h4></div>
                        <img src="images/gallery/small/02.jpg" alt="">
                    </div>

                    <div class="item wow zoomIn" data-wow-duration="0.50s" data-wow-delay="0.85s">
                        <div class="name">
                            <h1>Beautiful <span>Beaches</span></h1>
                            <a class="link" href="#" title=""><i class="fa fa-link"></i></a>
                            <h4>25 JAN<span>7 Days &amp; 6 Nights</span></h4></div>
                        <img src="images/gallery/small/03.jpg" alt="">
                    </div>

                    <div class="item">
                        <div class="name">
                            <h1>Adventure <span>Tour</span></h1>
                            <a class="link" href="#" title=""><i class="fa fa-link"></i></a>
                            <h4>25 JAN<span>7 Days &amp; 6 Nights</span></h4></div>
                        <img src="images/gallery/small/04.jpg" alt="">
                    </div>

                    <div class="item">
                        <div class="name">
                            <h1>Andaman <span>Island</span></h1>
                            <a class="link" href="#" title=""><i class="fa fa-link"></i></a>
                            <h4>25 JAN<span>7 Days &amp; 6 Nights</span></h4></div>
                        <img src="images/gallery/small/05.jpg" alt="">
                    </div>

                    <div class="item">
                        <div class="name">
                            <h1>Friendship <span>Tour</span></h1>
                            <a class="link" href="#" title=""><i class="fa fa-link"></i></a>
                            <h4>25 JAN<span>7 Days &amp; 6 Nights</span></h4></div>
                        <img src="images/gallery/small/06.jpg" alt="">
                    </div>

                    <div class="item">
                        <div class="name">
                            <h1>Honeymoon <span>Tour</span></h1>
                            <a class="link" href="#" title=""><i class="fa fa-link"></i></a>
                            <h4>25 JAN<span>7 Days &amp; 6 Nights</span></h4></div>
                        <img src="images/gallery/small/07.jpg" alt="">
                    </div>

                </div><!-- ::: Upcoming end ::: -->

            </div><!-- ::: col-md-12 end ::: -->
        </div>
    </div>
</section>
