<section id="gallery" class="page-block BGlight">
    <div class="container-wide">
        <h1 class="text-center wow zoomIn" data-wow-duration="0.50s" data-wow-delay="0.50s"><span>Photo</span> Gallery<small>Clix from Previous Tours</small></h1>

        <div id="galleryCarousel">

            <div class="item wow zoomIn" data-wow-duration="0.50s" data-wow-delay="0.50s">
                <img src="images/gallery/small/01.jpg" alt="">
                <div class="overlay">
                    <a class="zoom" href="images/gallery/large/01.jpg" data-rel="prettyPhoto[gallery2]" title="You can add caption to pictures."><img class="svg" src="images/svg/plus-icon.svg" onerror="this.src='plus-icon.png'" alt=""></a>
                </div>
            </div>

            <div class="item wow zoomIn" data-wow-duration="0.50s" data-wow-delay="0.65s">
                <img src="images/gallery/small/02.jpg" alt="">
                <div class="overlay">
                    <a class="zoom" href="images/gallery/large/02.jpg" data-rel="prettyPhoto[gallery2]" title="You can add caption to pictures."><img class="svg" src="images/svg/plus-icon.svg" onerror="this.src='plus-icon.png'" alt=""></a>
                </div>
            </div>

            <div class="item wow zoomIn" data-wow-duration="0.50s" data-wow-delay="0.80s">
                <img src="images/gallery/small/03.jpg" alt="">
                <div class="overlay">
                    <a class="zoom" href="images/gallery/large/03.jpg" data-rel="prettyPhoto[gallery2]" title="You can add caption to pictures."><img class="svg" src="images/svg/plus-icon.svg" onerror="this.src='plus-icon.png'" alt=""></a>
                </div>
            </div>

            <div class="item wow zoomIn" data-wow-duration="0.50s" data-wow-delay="0.95s">
                <img src="images/gallery/small/04.jpg" alt="">
                <div class="overlay">
                    <a class="zoom" href="images/gallery/large/04.jpg" data-rel="prettyPhoto[gallery2]" title="You can add caption to pictures."><img class="svg" src="images/svg/plus-icon.svg" onerror="this.src='plus-icon.png'" alt=""></a>
                </div>
            </div>

            <div class="item wow zoomIn" data-wow-duration="0.50s" data-wow-delay="1.10s">
                <img src="images/gallery/small/05.jpg" alt="">
                <div class="overlay">
                    <a class="zoom" href="images/gallery/large/05.jpg" data-rel="prettyPhoto[gallery2]" title="You can add caption to pictures."><img class="svg" src="images/svg/plus-icon.svg" onerror="this.src='plus-icon.png'" alt=""></a>
                </div>
            </div>

            <div class="item wow zoomIn" data-wow-duration="0.50s" data-wow-delay="0.50s">
                <img src="images/gallery/small/06.jpg" alt="">
                <div class="overlay">
                    <a class="zoom" href="images/gallery/large/06.jpg" data-rel="prettyPhoto[gallery2]" title="You can add caption to pictures."><img class="svg" src="images/svg/plus-icon.svg" onerror="this.src='plus-icon.png'" alt=""></a>
                </div>
            </div>

            <div class="item">
                <img src="images/gallery/small/07.jpg" alt="">
                <div class="overlay">
                    <a class="zoom" href="images/gallery/large/07.jpg" data-rel="prettyPhoto[gallery2]" title="You can add caption to pictures."><img class="svg" src="images/svg/plus-icon.svg" onerror="this.src='plus-icon.png'" alt=""></a>
                </div>
            </div>

            <div class="item">
                <img src="images/gallery/small/08.jpg" alt="">
                <div class="overlay">
                    <a class="zoom" href="images/gallery/large/08.jpg" data-rel="prettyPhoto[gallery2]" title="You can add caption to pictures."><img class="svg" src="images/svg/plus-icon.svg" onerror="this.src='plus-icon.png'" alt=""></a>
                </div>
            </div>

        </div>
    </div>
</section>
