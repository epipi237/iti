<section id="travelInfo" class="">
    <div class="page-block-large">
        <div class="container">
            <div class="row">

                <!-- ::: Owl Carousel Start ::: -->
                <div id="owl-demo">

                    <!-- ::: Start item ::: -->
                    <div class="item wow zoomIn" data-wow-duration="0.50s" data-wow-delay="0.50s">
                        <div class="item-header">
                            <i class="fa fa-plane"></i>
                            <h5>Flight Information<small>Departure and Arrival </small></h5>
                        </div><!-- end item-header -->

                        <div class="item-info">
                            <div class="departure">
                                <h6>DEPARTURE: <strong>25 JAN, 2018</strong></h6>
                                <div class="left">
                                    <h2>JFK</h2>
                                    New York, NY
                                    <strong>09.30 AM</strong>
                                </div>
                                <img class="svg arrPrime" src="images/svg/arrow-right-s.svg" onerror="this.src='images/svg/arrow-right-s.png'" alt="">
                                <div class="right">
                                    <h2>LAX</h2>
                                    Los Angeles
                                    <strong>09.30 AM</strong>
                                </div>
                                <div class="clear"></div>
                            </div><!-- end departure -->

                            <div class="arrival">
                                <h6>ARRIVAL: <strong>30 JAN, 2018</strong></h6>
                                <div class="left">
                                    <h2>LAX</h2>
                                    Los Angeles
                                    <strong>09.30 AM</strong>
                                </div>
                                <img class="svg arrPrime" src="images/svg/arrow-left-s.svg" onerror="this.src='images/svg/arrow-left-s.png'" alt="">
                                <div class="right">
                                    <h2>JFK</h2>
                                    New York, NY
                                    <strong>09.30 AM</strong>
                                </div>
                                <div class="clear"></div>
                            </div><!-- end arrival -->
                        </div><!-- end item-info -->
                    </div><!-- ::: End item ::: -->

                    <div class="item wow zoomIn" data-wow-duration="0.50s" data-wow-delay="0.65s">
                        <div class="item-header">
                            <i class="fa fa-train"></i>
                            <h5>TRAIN Information<small>Departure and Arrival </small></h5>
                        </div><!-- end item-header -->

                        <div class="item-info">
                            <div class="departure">
                                <h6>DEPARTURE: <strong>25 JAN, 2018</strong></h6>
                                <div class="left">
                                    <h5>Lorem Sta</h5>
                                    Avenue Thiers BP 1463<br>06008 Nice Cedex 1
                                    <strong>09.30 AM</strong>
                                </div>
                                <img class="svg arrPrime" src="images/svg/arrow-right-s.svg" onerror="this.src='images/svg/arrow-right-s.png'" alt="">
                                <div class="right">
                                    <h5>Ipsum Sta </h5>
                                    Avenue Thiers BP 1463<br>06008 Nice Cedex 1
                                    <strong>09.30 AM</strong>
                                </div>
                                <div class="clear"></div>
                            </div><!-- end departure -->

                            <div class="arrival">
                                <h6>ARRIVAL: <strong>30 JAN, 2018</strong></h6>
                                <div class="left">
                                    <h5>Lorem Sta</h5>
                                    Avenue Thiers BP 1463<br>06008 Nice Cedex 1
                                    <strong>09.30 AM</strong>
                                </div>
                                <img class="svg arrPrime" src="images/svg/arrow-left-s.svg" onerror="this.src='images/svg/arrow-left-s.png'" alt="">
                                <div class="right">
                                    <h5>Ipsum Sta </h5>
                                    Avenue Thiers BP 1463<br>06008 Nice Cedex 1
                                    <strong>09.30 AM</strong>
                                </div>
                                <div class="clear"></div>
                            </div><!-- end arrival -->
                        </div><!-- end item-info -->
                    </div><!-- ::: End item ::: -->

                    <div class="item wow zoomIn" data-wow-duration="0.50s" data-wow-delay="0.85s">
                        <div class="item-header">
                            <i class="fa fa-building"></i>
                            <h5>HOTEL Information<small>Accomodation and Dining</small></h5>
                        </div><!-- end item-header -->

                        <div class="item-info">
                            <img class="img-responsive" src="images/hotel.jpg" alt="">
                            <div class="info">
                                <h5 class="prime">Luxury Hotel</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipi scing elit. Donec feugiat iaculis tortor molestie. Nunc imperdiet commodo nunc, a porta eros iaculis sit amet iaculis tortor molestie iaculis tortor molestie.</p>
                                <a href="#" title=""><i class="fa fa-globe"></i> www.grandhotelinparis.com</a>
                            </div>
                        </div><!-- end item-info -->
                    </div><!-- ::: End item ::: -->

                    <div class="item">
                        <div class="item-header">
                            <i class="fa fa-ship"></i>
                            <h5>Cruise Information<small>Departure and Arrival </small></h5>
                        </div><!-- end item-header -->

                        <div class="item-info">
                            <div class="departure">
                                <h5>Royal Cruise 1234</h5>
                                <h6>DEPARTURE: <strong>25 JAN, 2018</strong></h6>
                                <div class="left">
                                    <h2>09.30</h2>
                                    Royal Port, USA
                                </div>
                                <img class="svg arrPrime" src="images/svg/arrow-right-s.svg" onerror="this.src='images/svg/arrow-right-s.png'" alt="">
                                <div class="right">
                                    <h2>02.30</h2>
                                    Royal Port, UK
                                </div>
                                <div class="clear"></div>
                            </div><!-- end departure -->

                            <div class="arrival">
                                <h5>Royal Cruise 5678</h5>
                                <h6>ARRIVAL: <strong>30 JAN, 2018</strong></h6>
                                <div class="left">
                                    <h2>14.30</h2>
                                    Royal Port, UK
                                </div>
                                <img class="svg arrPrime" src="images/svg/arrow-left-s.svg" onerror="this.src='images/svg/arrow-left-s.png'" alt="">
                                <div class="right">
                                    <h2>03.30</h2>
                                    Royal Port, USA
                                </div>
                                <div class="clear"></div>
                            </div><!-- end arrival -->
                        </div><!-- end item-info -->
                    </div><!-- ::: End item ::: -->

                    <div class="item">
                        <div class="item-header">
                            <i class="fa fa-cutlery"></i>
                            <h5>Restaurant Information<small>Accomodation and Dining</small></h5>
                        </div><!-- end item-header -->

                        <div class="item-info">
                            <img class="img-responsive" src="images/restaurant.jpg" alt="">
                            <div class="info">
                                <h5 class="prime">SpiceHub</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipi scing elit. Donec feugiat iaculis tortor molestie. Nunc imperdiet commodo nunc, a porta eros iaculis sit amet iaculis tortor molestie iaculis tortor molestie.</p>
                                <a href="#" title=""><i class="fa fa-globe"></i> www.grandhotelinparis.com</a>
                            </div>
                        </div><!-- end item-info -->
                    </div><!-- ::: End item ::: -->

                </div><!-- ::: end OWL Carousel ::: -->
            </div>
        </div>
    </div>
</section>
