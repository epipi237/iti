<section id="quotes" class="page-block text-center BGlight">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center wow zoomIn" data-wow-duration="0.50s" data-wow-delay="0.50s"><span>Client</span> Feedback<small>Words from our happy tourist</small></h1>

                <!-- ::: Testimonial slider Start ::: -->
                <div id="testimonial" class="carousel slide  wow zoomIn" data-wow-duration="0.65s" data-ride="carousel">
                    <div class="carousel-inner text-center">

                        <div class="item active">
                            <div class="heading"><h4>INCREDIBLE EXPERIENCE</h4></div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit Pellentesque elit tortor, adipiscing vel velit inermentum nulla. Donec in urna semulla facilisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tincidunt tellus vel libero pellentes Proin elit turpis, blandit in libero in</p>
                            <div class="author">
                                <img class="img-circle img-responsive" src="images/quote/01.jpg" width="65" height="65" alt="" /><h5>Natasha Romonoff <small>Secret Agent</small></h5>
                            </div>
                        </div><!-- end item -->

                        <div class="item ">
                            <div class="heading"><h4>Awesome Places</h4></div>
                            <p> Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam at mauris id massa gravida pulvinar vel Pellentesque elit tortor, adipiscing vel velit inermentum nulla. Donec in urna semulla facilisi. Liquam at mauris id massa gravida pulvinar</p>
                            <div class="author">
                                <img class="img-circle img-responsive" src="images/quote/02.jpg" width="65" height="65" alt="" /> <h5>Richard Simon <small>CEO at IT Company</small> </h5>
                            </div>
                        </div><!-- end item -->

                        <div class="item ">
                            <div class="heading"><h4>Best Food &amp; place</h4></div>
                            <p>Lliquam at mauris id massa gravida pulvina. Pellentesque elit tortor, adipiscing vel velit inermentum nulla. Donec in urna semulla facilisi.  Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam at mauris id massa gravida pulvinar velom.</p>
                            <div class="author">
                                <img class="img-circle img-responsive" src="images/quote/03.jpg" width="65" height="65" alt="" /> <h5>John Doen <small>CEO at IT Company</small> </h5>
                            </div>
                        </div> <!-- end item -->

                        <!-- Controls -->
                        <a class="left carousel-control" href="#testimonial" role="button" data-slide="prev">
                            <img class="svg arrPrime" src="images/svg/arrow-left-s.svg" onerror="this.src='images/svg/arrow-left-s.png'" alt="">
                        </a>
                        <a class="right carousel-control" href="#testimonial" role="button" data-slide="next">
                            <img class="svg arrPrime" src="images/svg/arrow-right-s.svg" onerror="this.src='images/svg/arrow-right-s.png'" alt="">
                        </a>

                    </div><!-- end carousel-inner -->
                </div><!-- end testimonial -->
            </div>
        </div><!-- end-row -->
    </div><!-- end-container -->
</section>
