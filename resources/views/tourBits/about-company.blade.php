<section id="about" class="page-block">
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-duration="0.50s" data-wow-delay="0.50s">
                <img class="img-responsive" src="images/company-green.png" width="350" height="142" alt="Company Logo" />
                <!--<h1>GroupTour<small>About company</small></h1>-->
                <ul class="list-inline social">
                    <li><a href="#" title="Folow me on Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" title="Folow me on Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" title="Watch me on Youtube" target="_blank"><i class="fa fa-youtube"></i></a></li>
                    <li><a href="#" title="LinkedIn" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#" title="Follow Us On Pinterest"><i class="fa fa-pinterest-square"></i></a></li>
                    <li><a href="#" title="Watch Our gallery"><i class="fa fa-flickr"></i></a></li>
                    <li><a href="#" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#" title="vk"><i class="fa fa-vk"></i></a></li>
                </ul>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInRight" data-wow-duration="0.50s" data-wow-delay="0.50s">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit Pellentesque elit tortor, adipiscing vel velit inermentum nulla. Donec in urna semulla facilisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tincidunt tellus vel libero pellentes Proin elit turpis, blandit in libero in</p> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit Pellentesque elite Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tincidunt tellus vel libero pellentes Proin elit turpis, blandit in libero in</p>
            </div><div class="clear"></div>

            <div class="additional-info">
                <div class="container wow fadeInUp" data-wow-duration="0.50s" data-wow-delay="0.50s">
                    <div class="col-md-4 col-sm-12 col-xs-12">
              <span><i class="fa fa-map-marker"></i> 123 Main Street<br>
                  Your City, State Country.
              </span>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <span class="phone"><i class="fa fa-phone"></i> (123) 456-7890 <br>(123) 456-7890</span>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <span><i class="fa fa-envelope"></i> <a href="mailto:contact@compnaynme.com" title="Contact">contact@compnaynme.com</a></span>
                    </div><div class="clear"></div>
                </div>
            </div>

        </div>
    </div>
</section>
