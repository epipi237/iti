<section class="page-block destination">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 wow fadeIn" data-wow-duration="0.50s" data-wow-delay="0.50s">
                <h1>{{ $tour->title }}<small>Deepest Place in the Ocean</small></h1>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 wow fadeIn" data-wow-duration="0.50s" data-wow-delay="0.50s">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed scelerisque, purus sit amet elementum blandit, sem arcu egestas quam, eget malesuada sem libero eu ante. Duis nec ultricies enim. Sed cursus volutpat finibus. Morbi at ornare purus. Vivamus congue suscipit dui nec fringilla. Nam auctor velit nec nisi molestie, ut maximus libero euismod. Nam dolor nunc, consequat nec sollicitudin vel, commodo vitae ex. Vestibulum rhoncus mollis felis in auctor. Donec at ultricies erat, eu pulvinar est. Integer pretium nunc quis dui sollicitudin commodo.</p>
            </div><div class="clear"></div>

            <!-- ::: Additional info Start ::: -->
            <div class="additional-info">
                <div class="container wow fadeInUp" data-wow-duration="0.50s" data-wow-delay="0.50s">
                    <div class="col-md-4 col-sm-12 col-xs-12">
              <span><i class="fa fa-calendar"></i> Adventure Begins<br>
                <strong>25 Jan 2018</strong>
              </span>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
              <span><i class="fa fa-bullhorn"></i> Duration<br>
                <strong>5 Days</strong>
              </span>
                    </div>
                    <div class="col-md-5 col-sm-12 col-xs-12">
              <span><i class="fa fa-cloud"></i> Wheather<br>
                <strong>Summer - 95<sup>&#8457;</sup> to 100.4<sup>&#8457;</sup></strong>
              </span>
                    </div><div class="clear"></div>
                </div>
            </div><!-- ::: Additional info end ::: -->
        </div>
    </div>
</section>
