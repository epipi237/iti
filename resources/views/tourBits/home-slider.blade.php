<!--  :::  HOME SLIDER ::: -->
<div id="slides" class="opt BGprime opaque">
    <!--<div class="texture"></div>-->
    <ul class="slides-container">
        <li>
            <img src="images/slider/slide1.jpg" alt="Slide 1">
        </li>
        <li>
            <img src="images/slider/slide2.jpg" alt="Slide 2">
        </li>
        <li>
            <img src="images/slider/slide3.jpg" alt="Slide 3">
        </li>
        <li>
            <img src="images/slider/slide4.jpg" alt="Slide 4">
        </li>
        <li>
            <img src="images/slider/slide5.jpg" alt="Slide 5">
        </li>
    </ul>

    <!-- COUNTER BOX & SUBSCRIBE -->
    <div class="info-box abs">
        <div class="container">
            <div class="col-md-6 col-sm-12 counter wow fadeInUp" data-wow-duration="0.50s" data-wow-delay="0.50s">
                <div class="date BGdark">DEPARTURE ON<h4>25 JAN <strong>2018</strong></h4></div>
                <div id="timer"></div>
                <div class="clear"></div>
            </div>
            <div class="col-md-6 col-sm-12 subscribe wow fadeInUp" data-wow-duration="0.50s" data-wow-delay="0.75s">
                <div class="BGlight">
                    <form action="form/subscribe.php" id="subscribeForm" method="post">
                        <div class="form-row">
                            <div class="left">
                                <div class="input"><input type="text" id="name1Subscribe" name="name1Subscribe" class="form-control" placeholder="First Name" /></div>
                                <div class="input"><input type="text" id="name2Subscribe" name="name2Subscribe" class="form-control" placeholder="Last Name" /></div>
                                <div class="input full"><input type="email" id="emailSubscribe" name="emailSubscribe" class="form-control" placeholder="Enter Your Email Address Here..." /></div>
                            </div>
                            <div class="submit right">
                                <input type="text" id="sub-security" name="sub-security" class="form-control hide" value="" />
                                <button type="submit" id="sub-submit" name="sub-submit" class="btn">Subscribe to Get Updates</button>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- ::: HOME SLIDER END ::: -->
