<section id="contact" class="">
    <div class="gradient dark">
        <div class="container">
            <div class="row">
                <div class="col-md-12 wow zoomIn" data-wow-duration="0.50s" data-wow-delay="0.50s">
                    <h1 class="text-center"><span>Have</span> Questions?<small>We’re here to help solve all your problems</small></h1>

                    <form id="contact_form" method="post" action="form/contact.php">
                        <div class="field-wrapper">
                            <div class="form-row col-md-6 col-sm-12">
                                <div><input type="text" class="form-control" id="name" name="name" placeholder="Your Name" /></div>
                                <div><input type="email" class="form-control" id="email" name="email" placeholder="Your Email" /></div>
                                <div><input type="text" class="form-control" id="phone" name="phone" placeholder="Your Phone" /></div>
                            </div>
                            <div class="form-row col-md-6 col-sm-12">
                                <textarea cols="60" rows="3" id="comment" name="comment" class="form-control" placeholder="Write your comment here..."></textarea>
                            </div><div class="clear"></div>
                            <div class="form-row  col-md-12">
                                <input type="text" id="security" name="security" class="form-control hide" value="" />
                                <input type="submit" value="&#xf1d8; SEND MESSAGE" class="btn btn-primary btn-lg" id="submit" name="submit" />
                            </div><div class="clear"></div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <footer class="BGdark opaque">
        <div class="container">
            <div class="col-md-6 col-sm-12">
                <p><strong>Group Tour</strong> &copy; 2016. All Rights Reserved.<br> Landing Page Template Designed &amp; Developed By: <a href="http://themeforest.net/user/saptarang?ref=saptarang" title="Saptarang"><strong>Saptarang</strong></a></p>
            </div>
            <div class="col-md-6 col-sm-12 text-right">
                <ul class="list-inline">
                    <li><a href="#" title="Folow me on Facebook" target="_blank"  data-toggle="modal" data-target="#myModal">Terms &amp; Conditions</a></li>
                </ul>
            </div>
        </div><!-- end container -->
    </footer>
</section>
