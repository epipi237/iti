<section id="itinarary" class="page-block BGprime">
    <div class="container">
        <div class="row">
            <div class="col-md-12 wow zoomIn" data-wow-duration="0.50s" data-wow-delay="0.50s">
                <h1 class="text-center"><span>TOUR</span> ITINERARY<small>Highlights Of Your Journey</small></h1>
            </div>
        </div>
    </div>

    <div class="tube">
        <span class="start">START</span>
        <span class="end">END</span>
    </div>
    <div id="car"></div>

    <!-- ::: Itinarary Start ::: -->
    <div class="itinarary">
        <div class="line"></div>

        <!-- ::: Place ::: -->
        <div class="day">
            <div class="left placeImg">
                <img src="images/places/01.jpg" width="800" height="480" alt="Place 1">
            </div>
            <div class="right placeInfo">
                <div class="dayNum abs">DAY 1</div>
                <h2>Snow Mountain city</h2>
                <span>Province / Snow Mountain</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed scelerisque, purus sit amet elementum blandit, sem arcu egestas quam, eget malesuada sem libero eu ante. Duis nec ultricies enim. Sed cursus volutpat finibus. Morbi at ornare purus. Vivamus congue suscipit dui nec fringilla. Nam auctor velit nec nisi molestie, ut maximus libero euismod. Nam dolor nunc, consequat nec sollicitudin vel, commodo vitae ex.</p>
                <ul class="special">
                    <li><i class="fa fa-map-marker"></i><h6>Tourist Attraction:</h6>Sunrise Point of Riverview</li>
                    <li><i class="fa fa-shopping-cart"></i><h6>Best Buy of this Place:</h6>Handmade Wooden Accessories</li>
                    <li><i class="fa fa-cutlery"></i><h6>Food Speciality:</h6>Sushie</li>
                    <li><i class="fa fa-street-view"></i><h6>Activity:</h6>Two Hours Boating Ride</li>
                </ul><div class="clear"></div>
                <div class="delight"><i class="fa fa-star"></i> <h6>TOUR DELIGHT : </h6>Dinner with Ice Cream Treat</div>
            </div><div class="clear"></div>
        </div><div class="clear"></div>

        <!-- ::: Place ::: -->
        <div class="day">
            <div class="left placeInfo">
                <div class="dayNum abs">DAY 2</div>
                <h2>Blue Sky city</h2>
                <span>Province / Blue Sky</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed scelerisque, purus sit amet elementum blandit, sem arcu egestas quam, eget malesuada sem libero eu ante. Duis nec ultricies enim. Sed cursus volutpat finibus. Morbi at ornare purus. Vivamus congue suscipit dui nec fringilla. Nam auctor velit nec nisi molestie, ut maximus libero euismod. Nam dolor nunc, consequat nec sollicitudin vel, commodo vitae ex.</p>
                <ul class="special">
                    <li><i class="fa fa-map-marker"></i><h6>Tourist Attraction:</h6>Sunrise Point of Riverview</li>
                    <li><i class="fa fa-shopping-cart"></i><h6>Best Buy of this Place:</h6>Handmade Wooden Accessories</li>
                    <li><i class="fa fa-cutlery"></i><h6>Food Speciality:</h6>Sushie</li>
                    <li><i class="fa fa-street-view"></i><h6>Activity:</h6>Two Hours Boating Ride</li>
                </ul><div class="clear"></div>
                <div class="delight"><i class="fa fa-star"></i> <h6>TOUR DELIGHT : </h6>Dinner with Ice Cream Treat</div>
            </div>
            <div class="right placeImg">
                <img src="images/places/02.jpg" width="800" height="480" alt="Place 2">
            </div><div class="clear"></div>
        </div><div class="clear"></div>

        <!-- ::: Place ::: -->
        <div class="day">
            <div class="left placeImg">
                <img src="images/places/03.jpg" width="800" height="480" alt="Place 3">
            </div>
            <div class="right placeInfo">
                <div class="dayNum abs">DAY 3</div>
                <h2>Lake city</h2>
                <span>Province / Lake City</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed scelerisque, purus sit amet elementum blandit, sem arcu egestas quam, eget malesuada sem libero eu ante. Duis nec ultricies enim. Sed cursus volutpat finibus. Morbi at ornare purus. Vivamus congue suscipit dui nec fringilla. Nam auctor velit nec nisi molestie, ut maximus libero euismod. Nam dolor nunc, consequat nec sollicitudin vel, commodo vitae ex.</p>
                <ul class="special">
                    <li><i class="fa fa-map-marker"></i><h6>Tourist Attraction:</h6>Sunrise Point of Riverview</li>
                    <li><i class="fa fa-shopping-cart"></i><h6>Best Buy of this Place:</h6>Handmade Wooden Accessories</li>
                    <li><i class="fa fa-cutlery"></i><h6>Food Speciality:</h6>Sushie</li>
                    <li><i class="fa fa-street-view"></i><h6>Activity:</h6>Two Hours Boating Ride</li>
                </ul><div class="clear"></div>
                <div class="delight"><i class="fa fa-star"></i> <h6>TOUR DELIGHT : </h6>Dinner with Ice Cream Treat</div>
            </div><div class="clear"></div>
        </div><div class="clear"></div>

        <!-- ::: Place ::: -->
        <div class="day">
            <div class="left placeInfo">
                <div class="dayNum abs">DAY 4</div>
                <h2>Awesome Beach</h2>
                <span>Province / Awesome</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed scelerisque, purus sit amet elementum blandit, sem arcu egestas quam, eget malesuada sem libero eu ante. Duis nec ultricies enim. Sed cursus volutpat finibus. Morbi at ornare purus. Vivamus congue suscipit dui nec fringilla. Nam auctor velit nec nisi molestie, ut maximus libero euismod. Nam dolor nunc, consequat nec sollicitudin vel, commodo vitae ex.</p>
                <ul class="special">
                    <li><i class="fa fa-map-marker"></i><h6>Tourist Attraction:</h6>Sunrise Point of Riverview</li>
                    <li><i class="fa fa-shopping-cart"></i><h6>Best Buy of this Place:</h6>Handmade Wooden Accessories</li>
                    <li><i class="fa fa-cutlery"></i><h6>Food Speciality:</h6>Sushie</li>
                    <li><i class="fa fa-street-view"></i><h6>Activity:</h6>Two Hours Boating Ride</li>
                </ul><div class="clear"></div>
                <div class="delight"><i class="fa fa-star"></i> <h6>TOUR DELIGHT : </h6>Dinner with Ice Cream Treat</div>
            </div>
            <div class="right placeImg">
                <img src="images/places/04.jpg" width="800" height="480" alt="Place 4">
            </div><div class="clear"></div>
        </div><div class="clear"></div>

        <!-- ::: Place ::: -->
        <div class="day">
            <div class="left placeImg">
                <img src="images/places/05.jpg" width="800" height="480" alt="Place 5">
            </div>
            <div class="right placeInfo">
                <div class="dayNum abs">DAY 5</div>
                <h2>Penguin Beach</h2>
                <span>Province / Penguin City</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed scelerisque, purus sit amet elementum blandit, sem arcu egestas quam, eget malesuada sem libero eu ante. Duis nec ultricies enim. Sed cursus volutpat finibus. Morbi at ornare purus. Vivamus congue suscipit dui nec fringilla. Nam auctor velit nec nisi molestie, ut maximus libero euismod. Nam dolor nunc, consequat nec sollicitudin vel, commodo vitae ex.</p>
                <ul class="special">
                    <li><i class="fa fa-map-marker"></i><h6>Tourist Attraction:</h6>Sunrise Point of Riverview</li>
                    <li><i class="fa fa-shopping-cart"></i><h6>Best Buy of this Place:</h6>Handmade Wooden Accessories</li>
                    <li><i class="fa fa-cutlery"></i><h6>Food Speciality:</h6>Sushie</li>
                    <li><i class="fa fa-street-view"></i><h6>Activity:</h6>Two Hours Boating Ride</li>
                </ul><div class="clear"></div>
                <div class="delight"><i class="fa fa-star"></i> <h6>TOUR DELIGHT : </h6>Dinner with Ice Cream Treat</div>
            </div><div class="clear"></div>
        </div><div class="clear"></div>

    </div><!-- ::: Itinarary end ::: -->
</section>
