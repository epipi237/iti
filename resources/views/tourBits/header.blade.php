<!-- ::: START HEADER ::: -->
<header id="home" class="affix">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12 logo">
                <a title="company Logo" href="index2.html"><img class="img-responsive" src="images/company-yellow.png" width="185" height="65" alt="Company Logo" /></a>
            </div>
            <div class="col-md-9 col-sm-12 col-xs-12 cbp-af-inner rightnav">
                <nav class="navbar navbar-default">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="menu">

                        <!-- ::: Main Nav ::: -->
                        <ul class="nav navbar-nav">
                            <li class="home active"><a href="index.html" title="Home"><i class="fa fa-home"></i></a></li>
                            <li><a href="#travelInfo" title="Travel Info">Travel Info</a></li>
                            <li><a href="#itinarary" title="Itinarary">Itinarary</a></li>
                            <li><a href="#gallery" title="Gallery">Gallery</a></li>
                            <li><a href="#quotes" title="Testimonials">Testimonials</a></li>
                            <li><a href="#about" title="About">About</a></li>
                            <li><a href="#contact" title="Contact">Contact</a></li>
                        </ul>
                        <!-- ::: Main Nav ::: -->

                    </div><!-- /.navbar-collapse -->
                </nav>
            </div>
        </div>
    </div><!-- end container -->
</header>
<!-- ::: END ::: -->