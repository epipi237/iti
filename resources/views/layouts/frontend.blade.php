<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <title>:: Welcome to Travelers Website ::</title>
    <meta name="description" content="Welcome to Travelers Landing Page Template">
    <meta name="keywords" content="traveler, travel, tour, vacation, beach, nature, place, world, back pack, ticket, event, visa, travel, tourism, green">
    <meta name="author" content="Saptarang">

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Google Font Code -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic&amp;subset=latin,greek,greek-ext,vietnamese,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>

    <!-- Stylesheets -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/prettyPhoto.css">
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/color-yellow.css" />

    <script src="js/modernizr.custom.js"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Favicon and Apple Icons -->
    <link rel="shortcut icon" href="images/icons/favicon.ico">
    <link rel="apple-touch-icon" href="images/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/icons/apple-icon-114x114.png">
</head>
<body id="top">
    @yield('content')
</body>
<a href="#top" class="top BGprime"><i class="fa fa-angle-up fa-lg"></i></a>
<!--START SCRIPTS -->
<script src="js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
<script src="js/jquery.countdown.js"></script>
<script src="js/jquery.superslides.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/placeholders.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/master.js"></script>
<script>
    // Full screen Slider
    $('#slides').superslides({
        animation: 'fade',
        play: 5000, // slideshow speed keep min 5000
        animation_speed: 7000,
        pagination: 'false'
    });
    // init controller CAR
    var controller = new ScrollMagic.Controller();
    var tube = $(".tube").outerHeight();
    var greyHgt = tube - 150;
    // create a scene
    var scene = new ScrollMagic.Scene({triggerElement: "#car", duration: greyHgt})
        .setPin("#car") // pins the element for the the scene's duration
        .addTo(controller); // assign the scene to the controller
</script>
</html>
