<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{slug}', 'tourController@show');

// Authentication Routes...
$this->get('auth/login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('auth/login', 'Auth\LoginController@login');
$this->post('auth/logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
$this->get('auth/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('auth/register', 'Auth\RegisterController@register');

// Password Reset Routes...
$this->get('auth/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('auth/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('auth/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
$this->post('auth/password/reset', 'Auth\ResetPasswordController@reset')->name('password.reset');

Route::get('/auth/home', 'DashboardController@index')->name('home');

// Tours Dashboard Routes...
Route::get('/auth/tours', 'tourController@index')->name('tours.index');
Route::get('/auth/tours/create', 'tourController@create')->name('tours.create');
